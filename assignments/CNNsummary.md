<div align="center">
<h1><b>CNN(Convolutional Neural Network)</b></h1>
</div>

<h2>Introduction</h2>

<div>
    CNN is an artificial neural network which contains convolution layers which processsing and identifies right output. It is the heart of the AI model. It is prefered to be used for classification problem for example is it a cat or dog, identifying hand wriiten numbers, etc.
</div>

<h2>CNN Architecture</h2>
<div>Don't panic from the image below, we will understand it step by step</div>

<img src="https://miro.medium.com/max/3744/1*SGPGG7oeSvVlV5sOSQ2iZw.png" />

<div>Above shown is a CNN architecture. We first feed an image to the network(in this case, handwritten number recognition).</div>

<h3>1. Filter</h3>
<div>
<ul>

<li>
Filter is the one which helps us identify patterns. It is a small(3*3 or 5*5) matrix which holds numbers denoting a particular feature to found in image such are eyes, ears. 
</li>

<li>
While programming, we set filter by kernel_size.
</li>

<li>
Each filter creates an emboss like effect from which the brightest pixeld are edges, shapes, corners detected. We can use multiple filters to identify more patterns for accuracy.
</li>

</ul>
</div>

<img src="https://tung2389.github.io/images/Machine%20learning/Neural%20network/Convolutional%20neural%20network/convolution%20layer.JPG" />

<h3>2. Convolution</h3>
<div>
<ul>

<li>
Convolution is traversing through the matrix. After selecting filters, we take each filter and convolve throughout the image matrix and store their dot product in a new matrix. 
</li>

<li>
This is the first and the output is further passed. Keras is an AI API with the help of which you can visualize this output.
</li>

</ul>
</div>

<h3>3. Zero Padding</h3>
<div>
<ul>

<li>
After you convolve one filter you will see the change the image dimensions reduced by 1. On convolving more different filter the image will dimish, to avoid this we use zero padding, which adds up zeros around are matrix.
</li>

<li>
There is no need to remember how many zero layers to add, it can be handled by code declaring padding = "same".(By default its "valid"). This will keep your image dimensions consistent.
</li>

</ul>
</div>

<h3>4. Max Pooling</h3>
<div>
<ul>

<li>
Max Pooling is a process in which we take input image and reduce the image pixels with losing it's important features.
</li>

<li>
First, we decide the filter(in this case, its small matrix in image ex. 3*3).
</li>

<li>
Secondly, we decide stride(which is a number to take jumps while convolving).
</li>

<li>
Third, we take max element from filter each time it moves and store it.
</li>

<li>
By selecting max element we do not lose high activation, reduce the computation power and fast processsing.
</li>

</ul>
</div>

<h3>5. Flattening</h3>
<div>
Flattening converts the the whole output of CNN into a single feature array. This is required to feed it to fully connected layer.
</div>

<h3>6. Classification</h3>
<div>
The last output layer has a classifier which identifies the output. It gives a probability for class.
</div>