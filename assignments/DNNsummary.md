<h1><b><div align="center">Deep Neural Network</div></b></h1>

<h2>1. What is neural networks?</h2>
<p>The word neural networks gives us an idea that it mimics the human brain structure. Here, the neuron a thing that holds a number between 0 & 1 which denotes the brightness of pixel and is called activation number. A network is connected of group of neurons. A deep neural network is used to make a model learn to identify patterns to automate the work.</p>

<h3>First Layer</h3>
<p>An image is an array of pixels which is considered as the first layer of our deep neural network. Each pixel has it's activation value in grayscale.</p>

<h3>Hidden Layer</h3>
<p>The hidden layer is the place where all the learning happens. In each layer, the network learns some patterns and thus that becomes identification factor.The number of hidden layers & neurons in it to be added is completely arbitary(You should keep on expermienting to find which suits the best for your network). The layers are connected through weights in between them(a number) If you want to set any condition for activation you can set bias.</p>

<h3>Output Layer</h3>
<p>The output layer has all the classes that the output could be. It also has neurons defining its activation. Based on the highest activation number the output is shown.</p>

<img src="https://miro.medium.com/max/6694/1*Enbag4OPicgOFGP6m281lQ.jpeg" />

<h2>How neural network learns?</h2>
<p>The neural networks learns by adjusting weights and bias simultaneously until you get your best accuracy.</p>

<h3>Cost Function</h3>
<p>The cost funtion is used to find error i.e., squared difference between expected neuron output and actual neurons value. Our goal is to minimize this cost function.</p>

<h3>Gradient Descent</h3>
<p>Gradient descent is used to minimize the cost funtion. When plotted the parameters, gradient descent  helps to find the local minima which will lead to decrease in cost function.
Stochastic Gradient descent is randomly dividing data into mini batches and then training over it repeadtedly to find local minma point with less computation.</p>

<img src="https://www.mltut.com/wp-content/uploads/2020/04/Untitled-document-3.png" />

<h3>Backpropagation</h3>
<p>Backpropagation is an algorithm in which each training example nudges the weights and biases, not only in just terms whether they should go up or down but also in terms of what relative properties to these changes cause the most rapid decrease to the cost.</p>

<div>To reach the perfect output, we can either change bias or weigths proportional activation.</div>
